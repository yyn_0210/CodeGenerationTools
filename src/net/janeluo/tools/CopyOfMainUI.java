package net.janeluo.tools;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import javax.swing.table.DefaultTableModel;

import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;

//VS4E -- DO NOT REMOVE THIS LINE!
public class CopyOfMainUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel templateNameLabel;
	private JTextField templateName;
	private JButton selectTemplateFileBtn;
	private JLabel outFolderLabel;
	private JTextField outFolder;
	private JButton selectOutFolderBtn;
	private JLabel jLabel0;
	private JTextField jTextField0;
	private JLabel jLabel1;
	private JTextField jTextField1;
	private JLabel jLabel2;
	private JComboBox jComboBox0;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JComboBox jComboBox1;
	private JButton jButton1;
	private JTabbedPane jTabbedPane0;
	private JTable jTable0;
	private JScrollPane jScrollPane0;
	private JLabel jLabel5;
	private JComboBox jComboBox2;
	private JTextField jTextField2;
	private static final String PREFERRED_LOOK_AND_FEEL = "javax.swing.plaf.metal.MetalLookAndFeel";
	public CopyOfMainUI() {
		initComponents();
	}

	private void initComponents() {
		setLayout(new GroupLayout());
		add(getTemplateNameLabel(), new Constraints(new Leading(24, 10, 10), new Leading(22, 10, 10)));
		add(getTemplateName(), new Constraints(new Leading(105, 324, 10, 10), new Leading(22, 12, 12)));
		add(getSelectTemplateFileBtn(), new Constraints(new Leading(432, 10, 10), new Leading(22, 22, 12, 12)));
		add(getOutFolderLabel(), new Constraints(new Leading(24, 12, 12), new Leading(62, 12, 12)));
		add(getOutFolder(), new Constraints(new Leading(105, 324, 12, 12), new Leading(62, 12, 12)));
		add(getJButton0(), new Constraints(new Leading(432, 12, 12), new Leading(62, 22, 12, 12)));
		add(getJTextField0(), new Constraints(new Leading(105, 151, 10, 10), new Leading(102, 12, 12)));
		add(getJLabel0(), new Constraints(new Leading(24, 12, 12), new Leading(104, 12, 12)));
		add(getJLabel1(), new Constraints(new Leading(280, 10, 10), new Leading(104, 12, 12)));
		add(getJTextField1(), new Constraints(new Leading(329, 40, 10, 10), new Leading(102, 12, 12)));
		add(getJLabel3(), new Constraints(new Leading(377, 132, 10, 10), new Leading(109, 12, 12)));
		add(getJComboBox0(), new Constraints(new Leading(123, 10, 10), new Leading(144, 22, 12, 12)));
		add(getJLabel2(), new Constraints(new Leading(24, 12, 12), new Leading(146, 12, 12)));
		add(getJTabbedPane0(), new Constraints(new Leading(24, 468, 12, 12), new Leading(232, 262, 10, 10)));
		add(getJLabel5(), new Constraints(new Leading(24, 12, 12), new Leading(191, 10, 10)));
		add(getJComboBox2(), new Constraints(new Leading(80, 10, 10), new Leading(188, 12, 12)));
		add(getJButton1(), new Constraints(new Leading(406, 12, 12), new Leading(187, 26, 12, 12)));
		add(getJComboBox1(), new Constraints(new Leading(366, 10, 10), new Leading(144, 22, 12, 12)));
		add(getJLabel4(), new Constraints(new Leading(280, 12, 12), new Leading(146, 12, 12)));
		add(getJTextField2(), new Constraints(new Leading(156, 114, 10, 10), new Leading(187, 12, 12)));
		setSize(519, 512);
	}

	private JTextField getJTextField2() {
		if (jTextField2 == null) {
			jTextField2 = new JTextField();
		}
		return jTextField2;
	}

	private JComboBox getJComboBox2() {
		if (jComboBox2 == null) {
			jComboBox2 = new JComboBox();
			jComboBox2.setModel(new DefaultComboBoxModel(new Object[] { "UTF-8", "GBK", "其他" }));
			jComboBox2.setDoubleBuffered(false);
			jComboBox2.setBorder(null);
		}
		return jComboBox2;
	}

	private JLabel getJLabel5() {
		if (jLabel5 == null) {
			jLabel5 = new JLabel();
			jLabel5.setText("字符集：");
		}
		return jLabel5;
	}

	private JScrollPane getJScrollPane0() {
		if (jScrollPane0 == null) {
			jScrollPane0 = new JScrollPane();
			jScrollPane0.setWheelScrollingEnabled(false);
			jScrollPane0.setViewportView(getJTable0());
		}
		return jScrollPane0;
	}

	private JTable getJTable0() {
		if (jTable0 == null) {
			jTable0 = new JTable();
			Vector<Vector<Object>> data = new Vector<Vector<Object>>();
			Vector<String> title = new Vector<String>();
			title.add("列1");
			title.add("列1");
			title.add("列1");
			title.add("列1");
			title.add("列1");
			jTable0.setModel(new DefaultTableModel(data, title) {
				private static final long serialVersionUID = 1L;
				Class<?>[] types = new Class<?>[] { Object.class, Object.class, };
	
				public Class<?> getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
		}
		return jTable0;
	}

	private JTabbedPane getJTabbedPane0() {
		if (jTabbedPane0 == null) {
			jTabbedPane0 = new JTabbedPane();
		}
		jTabbedPane0.add("选修",getJScrollPane0());
		
		
//		jTabbedPane0.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT); //设置选项卡的布局方式。
//		final JLabel tabLabelA = new JLabel();
//        tabLabelA.setText("选项卡A");
//        jTabbedPane0.addTab("选项卡A",tabLabelA);
//        final JLabel tabLabelB = new JLabel();
//        tabLabelB.setText("选项卡B");
//        jTabbedPane0.addTab("选项卡B",tabLabelB);
//        final JLabel tabLabelC = new JLabel();
//        tabLabelC.setText("选项卡C");
//        jTabbedPane0.addTab("选项卡C",tabLabelC);
//        jTabbedPane0.setSelectedIndex(0);  //设置默认选中的
		return jTabbedPane0;
	}

	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setText("生成代码");
			jButton1.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					jButton1MouseMouseClicked(event);
				}
			});
		}
		return jButton1;
	}

	private JComboBox getJComboBox1() {
		if (jComboBox1 == null) {
			jComboBox1 = new JComboBox();
			jComboBox1.setModel(new DefaultComboBoxModel(new Object[] { "是", "否" }));
			jComboBox1.setDoubleBuffered(false);
			jComboBox1.setBorder(null);
		}
		return jComboBox1;
	}

	private JLabel getJLabel4() {
		if (jLabel4 == null) {
			jLabel4 = new JLabel();
			jLabel4.setText("生产查询条件：");
		}
		return jLabel4;
	}

	private JLabel getJLabel3() {
		if (jLabel3 == null) {
			jLabel3 = new JLabel();
			jLabel3.setFont(new Font("Dialog", Font.PLAIN, 10));
			String str = "<html><font style=\"color:red;\">注：为空时表示没有模式</font></html>";
			jLabel3.setText(str);
		}
		return jLabel3;
	}

	private JComboBox getJComboBox0() {
		if (jComboBox0 == null) {
			jComboBox0 = new JComboBox();
			jComboBox0.setModel(new DefaultComboBoxModel(new Object[] { "是", "否" }));
			jComboBox0.setDoubleBuffered(false);
			jComboBox0.setBorder(null);
		}
		return jComboBox0;
	}

	private JLabel getJLabel2() {
		if (jLabel2 == null) {
			jLabel2 = new JLabel();
			jLabel2.setText("生成Spring注解：");
		}
		return jLabel2;
	}

	private JTextField getJTextField1() {
		if (jTextField1 == null) {
			jTextField1 = new JTextField();
		}
		return jTextField1;
	}

	private JLabel getJLabel1() {
		if (jLabel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("模式：");
		}
		return jLabel1;
	}

	private JTextField getJTextField0() {
		if (jTextField0 == null) {
			jTextField0 = new JTextField();
		}
		return jTextField0;
	}

	private JLabel getJLabel0() {
		if (jLabel0 == null) {
			jLabel0 = new JLabel();
			jLabel0.setText("生成包名：");
		}
		return jLabel0;
	}

	private JButton getJButton0() {
		if (selectOutFolderBtn == null) {
			selectOutFolderBtn = new JButton();
			selectOutFolderBtn.setText("选择");
		}
		return selectOutFolderBtn;
	}

	private JTextField getOutFolder() {
		if (outFolder == null) {
			outFolder = new JTextField();
		}
		return outFolder;
	}

	private JLabel getOutFolderLabel() {
		if (outFolderLabel == null) {
			outFolderLabel = new JLabel();
			outFolderLabel.setText("输入目录：");
		}
		return outFolderLabel;
	}

	private JButton getSelectTemplateFileBtn() {
		if (selectTemplateFileBtn == null) {
			selectTemplateFileBtn = new JButton();
			selectTemplateFileBtn.setText("选择");
		}
		return selectTemplateFileBtn;
	}

	private JTextField getTemplateName() {
		if (templateName == null) {
			templateName = new JTextField();
		}
		return templateName;
	}

	private JLabel getTemplateNameLabel() {
		if (templateNameLabel == null) {
			templateNameLabel = new JLabel();
			templateNameLabel.setText("模板文件：");
		}
		return templateNameLabel;
	}

	private static void installLnF() {
		try {
//			String lnfClassname = PREFERRED_LOOK_AND_FEEL;
			String lnfClassname = null;
			if (lnfClassname == null)
				lnfClassname = UIManager.getSystemLookAndFeelClassName();
			UIManager.setLookAndFeel(lnfClassname);
		} catch (Exception e) {
			System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL
					+ " on this platform:" + e.getMessage());
		}
	}

	/**
	 * Main entry of the class.
	 * Note: This class is only created so that you can easily preview the result at runtime.
	 * It is not expected to be managed by the designer.
	 * You can modify it as you like.
	 */
	public static void main(String[] args) {
		installLnF();
		InitGlobalFont(new Font("alias", Font.PLAIN, 12));  //统一设置字体
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				CopyOfMainUI frame = new CopyOfMainUI();
				frame.setDefaultCloseOperation(CopyOfMainUI.EXIT_ON_CLOSE);
				frame.setTitle("代码生成工具");
				frame.getContentPane().setPreferredSize(frame.getSize());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);
			}
		});
	}

	/**
	 * 统一设置字体，父界面设置之后，所有由父界面进入的子界面都不需要再次设置字体
	 */
	private static void InitGlobalFont(Font font) {
		FontUIResource fontRes = new FontUIResource(font);
		for (Enumeration<Object> keys = UIManager.getDefaults().keys(); keys
				.hasMoreElements();) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof FontUIResource) {
				UIManager.put(key, fontRes);
			}
		}
	}

	private void jButton1MouseMouseClicked(MouseEvent event) {
	}
}
