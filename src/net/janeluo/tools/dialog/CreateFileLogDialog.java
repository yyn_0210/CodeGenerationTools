package net.janeluo.tools.dialog;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import org.dyno.visual.swing.layouts.Bilateral;
import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;

//VS4E -- DO NOT REMOVE THIS LINE!
public class CreateFileLogDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jPanel0;
	private JLabel jLabel0;
	private JTextPane jTextPane0;
	private JScrollPane jScrollPane0;

	public CreateFileLogDialog(Window parent) {
		super(parent);
		initComponents();
	}

	public CreateFileLogDialog(Dialog parent, String title, boolean modal,
			GraphicsConfiguration arg) {
		super(parent, title, modal, arg);
		initComponents();
	}

	public CreateFileLogDialog(Dialog parent, String title, boolean modal) {
		super(parent, title, modal);
		initComponents();
	}

	public CreateFileLogDialog(Dialog parent, String title) {
		super(parent, title);
		initComponents();
	}

	public CreateFileLogDialog(Window parent, String title,
			ModalityType modalityType, GraphicsConfiguration arg) {
		super(parent, title, modalityType, arg);
		initComponents();
	}

	public CreateFileLogDialog(Window parent, String title,
			ModalityType modalityType) {
		super(parent, title, modalityType);
		initComponents();
	}

	public CreateFileLogDialog(Window parent, String title) {
		super(parent, title);
		initComponents();
	}

	public CreateFileLogDialog(Window parent, ModalityType modalityType) {
		super(parent, modalityType);
		initComponents();
	}

	public CreateFileLogDialog(Frame parent, String title) {
		super(parent, title);
		initComponents();
	}

	public CreateFileLogDialog(Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
	}

	public CreateFileLogDialog(Frame parent) {
		super(parent);
		initComponents();
	}

	public CreateFileLogDialog() {
		initComponents();
	}

	public CreateFileLogDialog(Dialog parent, boolean modal) {
		super(parent, modal);
		initComponents();
	}

	public CreateFileLogDialog(Dialog parent) {
		super(parent);
		initComponents();
	}

	public CreateFileLogDialog(Frame parent, String title, boolean modal,
			GraphicsConfiguration arg) {
		super(parent, title, modal, arg);
		initComponents();
	}

	public CreateFileLogDialog(Frame parent, String title, boolean modal) {
		super(parent, title, modal);
		initComponents();
	}

	private void initComponents() {
		setForeground(Color.black);
		setFont(new Font("Dialog", Font.PLAIN, 12));
		setLayout(new GroupLayout());
		add(getJPanel0(), new Constraints(new Bilateral(2, 0, 0), new Bilateral(2, 0, 0)));
		setSize(320, 240);
	}

	private JPanel getJPanel0() {
		if (jPanel0 == null) {
			jPanel0 = new JPanel();
			jPanel0.setLayout(new GroupLayout());
			jPanel0.add(getJLabel0(), new Constraints(new Leading(10, 10, 10), new Leading(6, 10, 10)));
			jPanel0.add(getJScrollPane0(), new Constraints(new Leading(7, 302, 10, 10), new Leading(30, 203, 12, 12)));
		}
		return jPanel0;
	}

	private JScrollPane getJScrollPane0() {
		if (jScrollPane0 == null) {
			jScrollPane0 = new JScrollPane();
			jScrollPane0.setViewportView(getJTextPane0());
		}
		return jScrollPane0;
	}

	private JTextPane getJTextPane0() {
		if (jTextPane0 == null) {
			jTextPane0 = new JTextPane();
		}
		return jTextPane0;
	}

	private JLabel getJLabel0() {
		if (jLabel0 == null) {
			jLabel0 = new JLabel();
			jLabel0.setText("��־");
		}
		return jLabel0;
	}

}
