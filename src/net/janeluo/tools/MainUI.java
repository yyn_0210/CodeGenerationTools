package net.janeluo.tools;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Enumeration;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.plaf.FontUIResource;
import javax.swing.table.DefaultTableModel;

import net.janeluo.tools.bean.TableModelBean;
import net.janeluo.tools.dialog.CreateFileLogDialog;
import net.janeluo.tools.service.MainUIService;

import org.apache.commons.lang.StringUtils;
import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
//VS4E -- DO NOT REMOVE THIS LINE!
public class MainUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel templateNameLabel;
	private JTextField templateName;
	private JButton selectTemplateFileBtn;
	private JLabel outFolderLabel;
	private JTextField outFolder;
	private JButton selectOutFolderBtn;
	private JLabel jLabel0;
	private JTextField jTextField0;
	private JLabel jLabel1;
	private JTextField schemaTF;
	private JLabel jLabel2;
	private JComboBox jComboBox0;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JComboBox jComboBox1;
	private JButton jButton1;
	private JTabbedPane jTabbedPane0;
	private JLabel jLabel5;
	private JComboBox characterSetType;
	private JTextField characterSetType_other;
	private static final String PREFERRED_LOOK_AND_FEEL = "javax.swing.plaf.metal.MetalLookAndFeel";
	
	private JFileChooser dlg;
	
	private List<TableModelBean> datalst = null;
	public MainUI() {
		dlg = new JFileChooser();
		initComponents();
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				int a = JOptionPane.showConfirmDialog(null, "确定关闭吗？", "温馨提示",
						JOptionPane.YES_NO_OPTION);
				if (a == 0) {
					System.exit(0); // 关闭
				}
			}
		});
	}

	private void initComponents() {
		setLayout(new GroupLayout());
		add(getTemplateNameLabel(), new Constraints(new Leading(24, 10, 10), new Leading(22, 10, 10)));
		add(getTemplateName(), new Constraints(new Leading(105, 324, 10, 10), new Leading(22, 12, 12)));
		add(getSelectTemplateFileBtn(), new Constraints(new Leading(432, 10, 10), new Leading(22, 22, 12, 12)));
		add(getOutFolderLabel(), new Constraints(new Leading(24, 12, 12), new Leading(62, 12, 12)));
		add(getOutFolder(), new Constraints(new Leading(105, 324, 12, 12), new Leading(62, 12, 12)));
		add(getJButton0(), new Constraints(new Leading(432, 12, 12), new Leading(62, 22, 12, 12)));
		add(getJTextField0(), new Constraints(new Leading(105, 151, 10, 10), new Leading(102, 12, 12)));
		add(getJLabel0(), new Constraints(new Leading(24, 12, 12), new Leading(104, 12, 12)));
		add(getJLabel1(), new Constraints(new Leading(280, 10, 10), new Leading(104, 12, 12)));
		add(getSchemaTF(), new Constraints(new Leading(329, 40, 10, 10), new Leading(102, 12, 12)));
		add(getJLabel3(), new Constraints(new Leading(377, 132, 10, 10), new Leading(109, 12, 12)));
		add(getJLabel2(), new Constraints(new Leading(24, 12, 12), new Leading(146, 12, 12)));
		add(getJTabbedPane0(), new Constraints(new Leading(24, 468, 12, 12), new Leading(232, 262, 10, 10)));
		add(getJLabel5(), new Constraints(new Leading(24, 12, 12), new Leading(191, 10, 10)));
		add(getCharacterSetType(), new Constraints(new Leading(80, 10, 10), new Leading(188, 12, 12)));
		add(getJButton1(), new Constraints(new Leading(406, 12, 12), new Leading(187, 26, 12, 12)));
		add(getJComboBox1(), new Constraints(new Leading(366, 10, 10), new Leading(144, 22, 12, 12)));
		add(getJLabel4(), new Constraints(new Leading(280, 12, 12), new Leading(146, 12, 12)));
		add(getCharacterSetType_other(), new Constraints(new Leading(156, 114, 10, 10), new Leading(187, 12, 12)));
		add(getJComboBox0(), new Constraints(new Leading(149, 86, 10, 10), new Leading(144, 22, 12, 12)));
		setSize(519, 512);
	}

	private JTextField getCharacterSetType_other() {
		if (characterSetType_other == null) {
			characterSetType_other = new JTextField();
			characterSetType_other.setVisible(false);
		}
		return characterSetType_other;
	}

	private JComboBox getCharacterSetType() {
		if (characterSetType == null) {
			characterSetType = new JComboBox();
			characterSetType.setModel(new DefaultComboBoxModel(new Object[] { "UTF-8", "GBK", "其他" }));
			characterSetType.setDoubleBuffered(false);
			characterSetType.setBorder(null);
			characterSetType.addItemListener(new ItemListener() {
	
				public void itemStateChanged(ItemEvent event) {
					characterSetTypeItemStateChanged(event);
				}
			});
		}
		return characterSetType;
	}

	private JLabel getJLabel5() {
		if (jLabel5 == null) {
			jLabel5 = new JLabel();
			jLabel5.setText("字符集：");
		}
		return jLabel5;
	}



	private JTabbedPane getJTabbedPane0() {
		if (jTabbedPane0 == null) {
			jTabbedPane0 = new JTabbedPane();
		}
		
		
		jTabbedPane0.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT); //设置选项卡的布局方式。
//		final JLabel tabLabelA = new JLabel();
//        tabLabelA.setText("选项卡A");
//        jTabbedPane0.addTab("选项卡A",tabLabelA);
//        final JLabel tabLabelB = new JLabel();
//        tabLabelB.setText("选项卡B");
//        jTabbedPane0.addTab("选项卡B",tabLabelB);
//        final JLabel tabLabelC = new JLabel();
//        tabLabelC.setText("选项卡C");
//        jTabbedPane0.addTab("选项卡C",tabLabelC);
//        jTabbedPane0.setSelectedIndex(0);  //设置默认选中的
		return jTabbedPane0;
	}

	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setText("生成代码");
			jButton1.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					jButton1MouseMouseClicked(event);
				}
			});
		}
		return jButton1;
	}

	private JComboBox getJComboBox1() {
		if (jComboBox1 == null) {
			jComboBox1 = new JComboBox();
			jComboBox1.setModel(new DefaultComboBoxModel(new Object[] { "是", "否" }));
			jComboBox1.setDoubleBuffered(false);
			jComboBox1.setBorder(null);
		}
		return jComboBox1;
	}

	private JLabel getJLabel4() {
		if (jLabel4 == null) {
			jLabel4 = new JLabel();
			jLabel4.setText("生产查询条件：");
		}
		return jLabel4;
	}

	private JLabel getJLabel3() {
		if (jLabel3 == null) {
			jLabel3 = new JLabel();
			jLabel3.setFont(new Font("Dialog", Font.PLAIN, 10));
			String str = "<html><font style=\"color:red;\">注：为空时表示没有模式</font></html>";
			jLabel3.setText(str);
		}
		return jLabel3;
	}

	private JComboBox getJComboBox0() {
		if (jComboBox0 == null) {
			jComboBox0 = new JComboBox();
			jComboBox0.setModel(new DefaultComboBoxModel(new Object[] { "配置文件", "注解" }));
			jComboBox0.setDoubleBuffered(false);
			jComboBox0.setBorder(null);
		}
		return jComboBox0;
	}

	private JLabel getJLabel2() {
		if (jLabel2 == null) {
			jLabel2 = new JLabel();
			jLabel2.setText("Spring依赖注入方式：");
		}
		return jLabel2;
	}

	private JTextField getSchemaTF() {
		if (schemaTF == null) {
			schemaTF = new JTextField();
		}
		return schemaTF;
	}

	private JLabel getJLabel1() {
		if (jLabel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("模式：");
		}
		return jLabel1;
	}

	private JTextField getJTextField0() {
		if (jTextField0 == null) {
			jTextField0 = new JTextField();
		}
		return jTextField0;
	}

	private JLabel getJLabel0() {
		if (jLabel0 == null) {
			jLabel0 = new JLabel();
			jLabel0.setText("生成包名：");
		}
		return jLabel0;
	}

	private JButton getJButton0() {
		if (selectOutFolderBtn == null) {
			selectOutFolderBtn = new JButton();
			selectOutFolderBtn.setText("选择");
			selectOutFolderBtn.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					selectOutFolderBtnMouseMouseClicked(event);
				}
			});
		}
		return selectOutFolderBtn;
	}

	private JTextField getOutFolder() {
		if (outFolder == null) {
			outFolder = new JTextField();
		}
		return outFolder;
	}

	private JLabel getOutFolderLabel() {
		if (outFolderLabel == null) {
			outFolderLabel = new JLabel();
			outFolderLabel.setText("输入目录：");
		}
		return outFolderLabel;
	}

	private JButton getSelectTemplateFileBtn() {
		if (selectTemplateFileBtn == null) {
			selectTemplateFileBtn = new JButton();
			selectTemplateFileBtn.setText("选择");
			selectTemplateFileBtn.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					selectTemplateFileBtnMouseMouseClicked(event);
				}
			});
		}
		return selectTemplateFileBtn;
	}

	private JTextField getTemplateName() {
		if (templateName == null) {
			templateName = new JTextField();
		}
		return templateName;
	}

	private JLabel getTemplateNameLabel() {
		if (templateNameLabel == null) {
			templateNameLabel = new JLabel();
			templateNameLabel.setText("模板文件：");
		}
		return templateNameLabel;
	}

	private static void installLnF() {
		try {
//			String lnfClassname = PREFERRED_LOOK_AND_FEEL;
			String lnfClassname = null;
			if (lnfClassname == null)
				lnfClassname = UIManager.getSystemLookAndFeelClassName();
			UIManager.setLookAndFeel(lnfClassname);
		} catch (Exception e) {
			System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL
					+ " on this platform:" + e.getMessage());
		}
	}

	/**
	 * Main entry of the class.
	 * Note: This class is only created so that you can easily preview the result at runtime.
	 * It is not expected to be managed by the designer.
	 * You can modify it as you like.
	 */
	public static void main(String[] args) {
		installLnF();
		InitGlobalFont(new Font("alias", Font.PLAIN, 12));  //统一设置字体
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				MainUI frame = new MainUI();
				frame.setDefaultCloseOperation(MainUI.EXIT_ON_CLOSE);
				frame.setTitle("代码生成工具");
				frame.getContentPane().setPreferredSize(frame.getSize());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);
				
			}
		});
	}

	/**
	 * 统一设置字体，父界面设置之后，所有由父界面进入的子界面都不需要再次设置字体
	 */
	private static void InitGlobalFont(Font font) {
		FontUIResource fontRes = new FontUIResource(font);
		for (Enumeration<Object> keys = UIManager.getDefaults().keys(); keys
				.hasMoreElements();) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof FontUIResource) {
				UIManager.put(key, fontRes);
			}
		}
	}

	private void jButton1MouseMouseClicked(MouseEvent event) {
		int a = JOptionPane.showConfirmDialog(null, "确定生成代码？", "温馨提示",
				JOptionPane.YES_NO_OPTION);
		if (a != 0) {
			return;
		}
		String tempName = templateName.getText();
		final String outFolderStr = outFolder.getText();
		final String packageName = jTextField0.getText();
		
		if (StringUtils.isEmpty(tempName)) {
			JOptionPane.showMessageDialog(null, "模板文件不能为空！", "错误提示",JOptionPane.ERROR_MESSAGE);
		} else if (StringUtils.isEmpty(outFolderStr)) {
			JOptionPane.showMessageDialog(null, "输出目录不能为空！", "错误提示",JOptionPane.ERROR_MESSAGE);
		} else if (StringUtils.isEmpty(packageName)) {
			JOptionPane.showMessageDialog(null, "生成包名不能为空！", "错误提示",JOptionPane.ERROR_MESSAGE);
		} else {
			SwingWorker<Void, Void> testWorker = new SwingWorker<Void , Void>(){

				private JDialog clueDiag = null;// “线程正在运行”提示框
				private Dimension dimensions = Toolkit.getDefaultToolkit().getScreenSize();
				private int width = dimensions.width / 4, height = 60;
				private String messages = "正在执行，请等待......";//提示框的提示信息
				
				@Override
				protected Void doInBackground() throws Exception {
					// TODO Auto-generated method stub
//					CreateFileLogDialog cfd = new CreateFileLogDialog(MainUI.this);
//					cfd.setModal(true);
//					cfd.setVisible(true);
					MainUIService rts = new MainUIService();
					clueDiag = new JDialog(MainUI.this,"正在执行，请等待...",true);
					clueDiag.setCursor(new Cursor(Cursor.WAIT_CURSOR));
					JPanel testPanel = new JPanel();
					JLabel testLabel = new JLabel(messages);
					clueDiag.getContentPane().add(testPanel);
					testPanel.add(testLabel);
					//显示提示框
					int left = (dimensions.width - width)/2;
					int top = (dimensions.height - height)/2;
					clueDiag.setSize(new Dimension(width,height));
					clueDiag.setLocation(left, top);
					clueDiag.setDefaultCloseOperation(MainUI.NORMAL);
					clueDiag.setResizable(false);
					clueDiag.setModal(true);
					Runnable run = new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							clueDiag.setVisible(true);
							
						}
					};
					new Thread(run).start();
//					clueDiag.show();
					
					// 字符集
					String characterSet = characterSetType.getSelectedItem().toString();
					if ("".equals(characterSet)) {
						characterSet = characterSetType_other.getText();
					}
					rts.setCharacterSet(characterSet);
					rts.setAopType(jComboBox0.getSelectedItem().toString());
					rts.setCreateCond(jComboBox1.getSelectedItem().toString());
					rts.setSchema(getSchemaTF().getText());
					
					if (null != datalst && datalst.size() > 0) {
						rts.createCode(outFolderStr, packageName, datalst);
					}
					System.out.println("-------11111111---------");
					clueDiag.setVisible(false);
					return null;
				}  
				
				protected void done(){   
					JOptionPane.showMessageDialog(null, "代码生成成功！", "提示",JOptionPane.QUESTION_MESSAGE);
				}
				
			};
			      
			//execute方法是异步执行，它立即返回到调用者。在execute方法执行后，EDT立即继续执行  
			testWorker.execute();
			
		}
	}

	private void selectTemplateFileBtnMouseMouseClicked(MouseEvent event) {
		
		dlg.setDialogTitle("选择模板文件");
		dlg.setFileSelectionMode(JFileChooser.FILES_ONLY);
		dlg.setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				// TODO Auto-generated method stub
				return ".xls";
			}
			
			@Override
			public boolean accept(File f) {
				if (f.isDirectory())return true;
			    return f.getName().endsWith(".xls");  //设置为选择以.class为后缀的文件
			}
		});
		int result = dlg.showOpenDialog(this);  // 打开"打开文件"对话框
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = dlg.getSelectedFile();
			templateName.setText(file.getAbsolutePath());
			jTabbedPane0.removeAll();
			MainUIService rts = new MainUIService();
			try {
				datalst = rts.readTemplate(file.getAbsolutePath());
				for (TableModelBean tableModelBean : datalst) {
					

					JTable table = new JTable();
					table.setModel(new DefaultTableModel(tableModelBean.getData(), tableModelBean.getColumnNames()));
					
					JScrollPane scrollPane = new JScrollPane();
					scrollPane.setWheelScrollingEnabled(false);
					scrollPane.setViewportView(table);
					
					jTabbedPane0.add(tableModelBean.getLogicalname(), scrollPane);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void selectOutFolderBtnMouseMouseClicked(MouseEvent event) {
		dlg.setDialogTitle("选择输出目录");
		dlg.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int result = dlg.showOpenDialog(this);  // 打开"打开文件"对话框
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = dlg.getSelectedFile();
			outFolder.setText(file.getAbsolutePath());
		}
	}

	private void characterSetTypeItemStateChanged(ItemEvent event) {
		if (event.getStateChange() == ItemEvent.SELECTED) {
			System.out.println(event.getItem().toString());
			if ("其他".equals(event.getItem().toString())) {
				characterSetType_other.setVisible(true);
			} else {
				characterSetType_other.setVisible(false);
			}
		}
	}

}
