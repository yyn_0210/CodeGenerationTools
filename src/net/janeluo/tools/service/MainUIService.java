package net.janeluo.tools.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import net.janeluo.tools.bean.TableModelBean;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class MainUIService {
	
//	private final static String BASEPACKAGE = "北京递蓝科软件技术有限公司";
	private final static String BASEPACKAGE = "简洛软件工作室";
	private final static String BASEPACKAGE_FRAMEWORK = "net.janeluo.cloud.framework";
	private final static String BASEPACKAGE_SERVICE = "service";
//	private final static String BASEPACKAGE_SERVICE = "bl";
	private final static String BASENAME_SERVICE = "Service";
//	private final static String BASENAME_SERVICE = "BL";
	
	private String characterSet = "GBK";
	
	private String schema;
	
	private String aopType;
	
	private String createCond;
	
	public void setCharacterSet(String characterSet){
		this.characterSet = characterSet;
	}
	
	public String getCharacterSet(){
		return this.characterSet;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getAopType() {
		return aopType;
	}

	public void setAopType(String aopType) {
		this.aopType = aopType;
	}

	public String getCreateCond() {
		return createCond;
	}

	public void setCreateCond(String createCond) {
		this.createCond = createCond;
	}

	/**
	 * 读取模板文件
	 * 
	 * @param filePath
	 * @throws BiffException
	 * @throws IOException
	 */
	public List<TableModelBean> readTemplate(String filePath) throws BiffException, IOException {
		// 创建一个list 用来存储读取的内容
		List<TableModelBean> list = new ArrayList<TableModelBean>();
		Cell cell = null;

		// 创建输入流
		InputStream stream = new FileInputStream(filePath);
		// 获取Excel文件对象
		Workbook rwb = Workbook.getWorkbook(stream);
		// 取得sheet页集合
		Sheet[] sheets = rwb.getSheets();
		for (Sheet sheet : sheets) {
			TableModelBean tmb = new TableModelBean();
			Map<String, String> pkMap = new HashMap<String, String>();
			
			// 定义列标题
			Vector<Object> columnNames = new Vector<Object>();
			columnNames.add("logical name");
			columnNames.add("physical name");
			columnNames.add("Type");
			columnNames.add("Length");
			columnNames.add("PK");
			tmb.setColumnNames(columnNames);
			
			// 逻辑名
			cell = sheet.getCell(1, 0);
			String logicalname = cell.getContents();
			tmb.setLogicalname(logicalname);
			// 物理名
			cell = sheet.getCell(1, 1);
			String physicalname = cell.getContents();
			tmb.setPhysicalname(physicalname);
			
			int dataindex = 6;				// 数据开始行数
			int cols = sheet.getRows();	// 总行数
			
			Vector<Vector<Object>> data = new Vector<Vector<Object>>();
			for (int i = dataindex; i < cols; i++) {
				Vector<Object> colVec = new Vector<Object>();
				// 列逻辑名
				cell = sheet.getCell(0, i);
				String collogicalname = cell.getContents();
				if (null == collogicalname || "".equals(collogicalname)) {
					break;
				}
				colVec.add(collogicalname);
				// 列物理名
				cell = sheet.getCell(1, i);
				String colphysicalname = cell.getContents();
				colVec.add(colphysicalname);
				// 字段类型
				cell = sheet.getCell(2, i);
				String coltype = cell.getContents();
				colVec.add(coltype);
				// 字段长度
				cell = sheet.getCell(3, i);
				String collength = cell.getContents();
				colVec.add(collength);
				// 主键
				cell = sheet.getCell(5, i);
				String colpk = cell.getContents().trim();
				colVec.add(colpk);
				data.add(colVec);
				if (StringUtils.isNotEmpty(colpk)) {
					pkMap.put(colphysicalname, changePropertyType(coltype));
				}
			}
			tmb.setPkMap(pkMap);
			tmb.setData(data);
			list.add(tmb);
		}
		if (rwb != null) {
			rwb.close();
		}
		if (stream != null) {
			stream.close();
		}
		return list;
	}
	
	public void createCode(String outFolder, String packageName, List<TableModelBean> lst){
		for (TableModelBean tableModelBean : lst) {
			// 主键Map
			Map<String, String> pkMap = tableModelBean.getPkMap();
			int complexPK = pkMap.size();
			if (complexPK == 1) {
				for (Map.Entry<String, String> entry : pkMap.entrySet()) {
					if (!changePropertyType(entry.getValue()).equals("Integer")) {
						complexPK = 0;
					}
				}
			}
			
			// 创建接口
			createInfo(outFolder, packageName, tableModelBean);
			// 创建实体
			createTB(outFolder, packageName, tableModelBean, complexPK);
			// 生成cond接口
			createCond(outFolder, packageName, tableModelBean);
			// 生成cond接口实现
			createCondImpl(outFolder, packageName, tableModelBean);
			// 生成DAO接口
			createDAO(outFolder, packageName, tableModelBean);
			// 生成DAO实现
			createDAOImpl(outFolder, packageName, tableModelBean);
			// 生成taskService接口
			createTaskService(outFolder, packageName, tableModelBean, complexPK);
			// 生成taskService接口实现
			createTaskServiceImpl(outFolder, packageName, tableModelBean, complexPK);
		}
		if ("配置文件".equals(getAopType())) {
			createApplicationXML(outFolder, packageName, lst);
		}
	}
	
	/**
	 * 生成Info接口
	 * @param bean
	 */
	private void createInfo(String outFolder, String packageName, TableModelBean bean){
		StringBuffer content = new StringBuffer();
		String classComment = bean.getLogicalname();
		String className = toUpperCaseFirstOne(bean.getPhysicalname()) + "Info";
		content.append(getTitle());
		content.append("\n");
		content.append("package " + packageName + ".model;");
		content.append("\n\r");
		content.append("import "  + BASEPACKAGE_FRAMEWORK + ".base.BaseModel;");
		content.append("\n\r");
		content.append("/** \n");
		content.append(" * 概述: " + classComment + "接口\n");
		content.append(" * 功能描述: " + classComment + "接口\n");
		content.append(" * 创建记录: \n");
		content.append(" * 修改记录: \n");
		content.append(" */\n");
		content.append("public interface " + className + " extends BaseModel {\n");
		content.append("\r");
		Vector<Vector<Object>> data = bean.getData();
		for (Vector<Object> vector : data) {
			content.append("    /**\n");
			content.append("     * " + vector.get(0) + "取得\n");
			content.append("     *\n");
			content.append("     * @return " + vector.get(0) + "\n");
			content.append("     */\n");
			content.append("    public " + changePropertyType(vector.get(2).toString()) + " get" + toUpperCaseFirstOne(vector.get(1).toString()) + "();\n");
			content.append("\r");
		}
		content.append("}");
		
		// 写入文件
		writeFile(outFolder, packageName + ".model", className + ".java", content); 
	}
	
	/**
	 * 生成TB实现
	 * 
	 * @param outFolder
	 * @param packageName
	 * @param bean
	 * @param complexPK
	 */
	private void createTB(String outFolder, String packageName, TableModelBean bean, Integer complexPK){
		
		Vector<Vector<Object>> data = bean.getData();
		// 主键集合
		List<String> pkLst = new LinkedList<String>();
		
		StringBuffer content = new StringBuffer();
		String classComment = bean.getLogicalname();
		String className = toUpperCaseFirstOne(bean.getPhysicalname());
		content.append(getTitle());
		content.append("\n");
		content.append("package " + packageName + ".model.entity;");
		content.append("\n\r");
		content.append("import java.io.Serializable;\n");
		content.append("import javax.persistence.Entity;\n");
		if (complexPK == 1) {
			content.append("import javax.persistence.GeneratedValue;\n");
			content.append("import javax.persistence.GenerationType;\n");
			content.append("import javax.persistence.Id;\n");
			content.append("import javax.persistence.Table;\n");
			content.append("import javax.persistence.TableGenerator;\n");
		} else {
			content.append("import javax.persistence.Id;").append("\n");
			content.append("import javax.persistence.IdClass;").append("\n");
			content.append("import javax.persistence.Table;").append("\n");
		}
		content.append("import  "  + BASEPACKAGE_FRAMEWORK + ".base.BaseEntity;").append("\n");
		content.append("import " + packageName + ".model." + className + "Info;").append("\n");
		content.append("\n\r");
		content.append("/** \n");
		content.append(" * 概述: " + classComment + "接口\n");
		content.append(" * 功能描述: " + classComment + "接口\n");
		content.append(" * 创建记录: \n");
		content.append(" * 修改记录: \n");
		content.append(" */\n");
		
		
		content.append("@Entity" ).append("\n");
		// 生成复合主键
		if (complexPK > 1) {
			content.append("@IdClass(" + className + "PK.class)" ).append("\n");
			createPK(outFolder, packageName, bean);
		}
		content.append("@Table(name=\"" + bean.getPhysicalname() );
		if (StringUtils.isNotEmpty(getSchema())) {
			content.append("\", schema=\"").append(getSchema());
		}
		content.append("\")" ).append("\n");
		content.append("@SuppressWarnings(\"serial\")" ).append("\n");
		content.append("public class " + className + "TB extends BaseEntity implements Serializable,").append("\n");
		content.append("       Cloneable, " + className + "Info {").append("\n");
		
		// 生产属性
		for (Vector<Object> vector : data) {
			String propertyName = vector.get(1).toString();//定义属性名
			String propertyType = vector.get(2).toString(); //定义属性类型
			String propertyComment = vector.get(0).toString();  //定义属性注释
			String propertyPK = vector.get(4).toString().trim();  //定义主键
			if (StringUtils.isNotEmpty(propertyName) && StringUtils.isNotEmpty(propertyType)
					&& StringUtils.isNotEmpty(propertyComment) && !"CREATETIME".equals(propertyName.toUpperCase()) 
					&& !"UPDATETIME".equals(propertyName.toUpperCase())) {
				content.append("\r");
				content.append("    /** " + propertyComment + " */").append("\n");
				if (StringUtils.isNotEmpty(propertyPK) ) {
					content.append("    @Id").append("\n"); 
					if (complexPK == 1 && changePropertyType(propertyType).equals("Integer")) {
						content.append("    @TableGenerator(").append("\n");
						content.append("        name=\""  + className.toUpperCase() + "_TABLE_GENERATOR\", table=\"SEQUENCE_GENERATOR_TABLE\",").append("\n");
						content.append("        pkColumnName=\"SEQUENCE_NAME\", valueColumnName=\"SEQUENCE_VALUE\",").append("\n");
						content.append("        pkColumnValue=\"" + className.toUpperCase() + "_SEQUENCE\", initialValue=1, allocationSize=1").append("\n");
						content.append("    )").append("\n");
						content.append("    @GeneratedValue(strategy = GenerationType.TABLE, generator=\"" + className.toUpperCase() + "_TABLE_GENERATOR\")").append("\n");
					}
					pkLst.add(propertyName);
				}
				content.append("    protected " + changePropertyType(propertyType) + " " + propertyName + ";").append("\n");
			}
			
		}

		// 生产属性对应的Get、Set方法
		for (Vector<Object> vector : data) {
			String propertyName = vector.get(1).toString();//定义属性名
			String propertyType = vector.get(2).toString(); //定义属性类型
			String propertyComment = vector.get(0).toString();  //定义属性注释
			if (StringUtils.isNotEmpty(propertyName) && StringUtils.isNotEmpty(propertyType)
					&& StringUtils.isNotEmpty(propertyComment) && !"CREATETIME".equals(propertyName.toUpperCase()) 
					&& !"UPDATETIME".equals(propertyName.toUpperCase())) {
				createGetSetMethod(content, propertyName, propertyType,
						propertyComment);
			}
		}
		
		// 生成PK方法
		content.append("    /**").append("\n");
		content.append("     * 主键取得").append("\r");
		content.append("     * @return 主键").append("\n");
		content.append("     */").append("\n");
		content.append("    public Serializable getPK() {").append("\n");
		if (complexPK <= 1) {
			content.append("        return " + pkLst.get(0) + ";").append("\n");
		} else if (complexPK > 1) {
			StringBuffer pksb = new StringBuffer();
			for (String pk : pkLst) {
				pksb.append(pk + ", ");
				
			}
			content.append("        return new " + className + "PK(" + pksb.substring(0, pksb.lastIndexOf(",")) + ");").append("\n");
		}
		content.append("    }").append("\n");
		
		content.append("}").append("\n");
		
		// 写入文件
		writeFile(outFolder, packageName + ".model.entity", className + "TB.java", content); 
	}

	/**
	 * 生成cond接口
	 * 
	 * @param outFolder
	 * @param packageName
	 * @param bean
	 */
	private void createCond(String outFolder, String packageName, TableModelBean bean){
		StringBuffer content = new StringBuffer();
		String classComment = bean.getLogicalname();
		String className = toUpperCaseFirstOne(bean.getPhysicalname());
		content.append(getTitle());
		content.append("\n");
		content.append("package " + packageName + ".model.condition;");
		content.append("\n\r");
		content.append("\n\r");
		content.append("/** \n");
		content.append(" * 概述: " + classComment + "检索条件接口\n");
		content.append(" * 功能描述: " + classComment + "检索条件接口\n");
		content.append(" * 创建记录: \n");
		content.append(" * 修改记录: \n");
		content.append(" */\n");
		content.append("public interface " + className + "SearchCond {\n");
		content.append("\r");
		if ("是".equals(getCreateCond())) {
			Vector<Vector<Object>> data = bean.getData();
			for (Vector<Object> vector : data) {
				String propertyType = changePropertyType(vector.get(2).toString());
				String propertyName = toUpperCaseFirstOne(vector.get(1).toString());
				if (StringUtils.isNotEmpty(propertyName) && StringUtils.isNotEmpty(propertyType)
						&& !"CREATETIME".equals(propertyName.toUpperCase()) && !"UPDATETIME".equals(propertyName.toUpperCase())) {
					content.append("    /**\n");
					content.append("     * " + vector.get(0) + "全等取得\n");
					content.append("     *\n");
					content.append("     * @return " + vector.get(0) + "\n");
					content.append("     */\n");
					content.append("    public " + propertyType + " get" + propertyName + "_eq();\n");
					content.append("\r");
					
					content.append("    /**\n");
					content.append("     * " + vector.get(0) + "不等取得\n");
					content.append("     *\n");
					content.append("     * @return " + vector.get(0) + "\n");
					content.append("     */\n");
					content.append("    public " + propertyType + " get" + propertyName + "_ne();\n");
					content.append("\r");
					
					content.append("    /**\n");
					content.append("     * " + vector.get(0) + "包含取得\n");
					content.append("     *\n");
					content.append("     * @return " + vector.get(0) + "\n");
					content.append("     */\n");
					content.append("    public " + propertyType + " get" + propertyName + "_in();\n");
					content.append("\r");
					
					content.append("    /**\n");
					content.append("     * " + vector.get(0) + "不包含取得\n");
					content.append("     *\n");
					content.append("     * @return " + vector.get(0) + "\n");
					content.append("     */\n");
					content.append("    public " + propertyType + " get" + propertyName + "_notin();\n");
					content.append("\r");
					
					if ("Integer".equals(propertyType)) {
						content.append("    /**\n");
						content.append("     * " + vector.get(0) + "大于取得\n");
						content.append("     *\n");
						content.append("     * @return " + vector.get(0) + "\n");
						content.append("     */\n");
						content.append("    public " + propertyType + " get" + propertyName + "_ge();\n");
						content.append("\r");
						content.append("    /**\n");
						content.append("     * " + vector.get(0) + "小于取得\n");
						content.append("     *\n");
						content.append("     * @return " + vector.get(0) + "\n");
						content.append("     */\n");
						content.append("    public " + propertyType + " get" + propertyName + "_le();\n");
						content.append("\r");
					}
				}
				
			}
		}
		content.append("}");
		
		// 写入文件
		writeFile(outFolder, packageName + ".model.condition", className + "SearchCond.java", content); 
	}

	/**
	 * 生成cond实现
	 * 
	 * @param outFolder
	 * @param packageName
	 * @param bean
	 */
	private void createCondImpl(String outFolder, String packageName, TableModelBean bean){
		
		Vector<Vector<Object>> data = bean.getData();
		
		StringBuffer content = new StringBuffer();
		String classComment = bean.getLogicalname();
		String className = toUpperCaseFirstOne(bean.getPhysicalname());
		content.append(getTitle());
		content.append("\n");
		content.append("package " + packageName + ".model.condition;");
		content.append("\n\r");
//		content.append("import " + packageName + ".model." + className + "Info;").append("\n");
//		content.append("\n\r");
		content.append("/** \n");
		content.append(" * 概述: " + classComment + "检索条件实现\n");
		content.append(" * 功能描述: " + classComment + "检索条件实现\n");
		content.append(" * 创建记录: \n");
		content.append(" * 修改记录: \n");
		content.append(" */\n");
		
		content.append("public class " + className + "SearchCondImpl implements ").append("\n");
		content.append("       " + className + "SearchCond {").append("\n");
		
		if ("是".equals(getCreateCond())) {
			// 生产属性
			for (Vector<Object> vector : data) {
				String propertyName = vector.get(1).toString();//定义属性名
				String propertyType = vector.get(2).toString(); //定义属性类型
				String propertyComment = vector.get(0).toString();  //定义属性注释
				if (StringUtils.isNotEmpty(propertyName) && StringUtils.isNotEmpty(propertyType)
						&& StringUtils.isNotEmpty(propertyComment) && !"CREATETIME".equals(propertyName.toUpperCase()) 
						&& !"UPDATETIME".equals(propertyName.toUpperCase())) {
					propertyType = changePropertyType(propertyType);
					content.append("\r");
					content.append("    /** " + propertyComment + "全等 */").append("\n");
					content.append("    protected " + propertyType + " " + propertyName + "_eq;").append("\n");
					
					content.append("\r");
					content.append("    /** " + propertyComment + "不等 */").append("\n");
					content.append("    protected " + propertyType + " " + propertyName + "_ne;").append("\n");
					
					content.append("\r");
					content.append("    /** " + propertyComment + "包含 */").append("\n");
					content.append("    protected " + propertyType + " " + propertyName + "_in;").append("\n");
					
					content.append("\r");
					content.append("    /** " + propertyComment + "不包含 */").append("\n");
					content.append("    protected " + propertyType + " " + propertyName + "_notin;").append("\n");
					
					if ("Integer".equals(propertyType)) {
						
						content.append("\r");
						content.append("    /** " + propertyComment + "大于 */").append("\n");
						content.append("    protected " + propertyType + " " + propertyName + "_ge;").append("\n");
						
						content.append("\r");
						content.append("    /** " + propertyComment + "小于 */").append("\n");
						content.append("    protected " + propertyType + " " + propertyName + "_le;").append("\n");
						
					}
					
				}
				
			}
			
			// 生产属性对应的Get、Set方法
			for (Vector<Object> vector : data) {
				String propertyName = vector.get(1).toString();//定义属性名
				String propertyType = vector.get(2).toString(); //定义属性类型
				String propertyComment = vector.get(0).toString();  //定义属性注释
				if (StringUtils.isNotEmpty(propertyName) && StringUtils.isNotEmpty(propertyType)
						&& StringUtils.isNotEmpty(propertyComment) && !"CREATETIME".equals(propertyName.toUpperCase()) 
						&& !"UPDATETIME".equals(propertyName.toUpperCase())) {
					
					createGetSetMethod(content, propertyName + "_eq", propertyType, propertyComment + "全等");
					createGetSetMethod(content, propertyName + "_ne", propertyType, propertyComment + "不等");
					createGetSetMethod(content, propertyName + "_in", propertyType, propertyComment + "包含");
					createGetSetMethod(content, propertyName + "_notin", propertyType, propertyComment + "不包含");
					
					if ("Integer".equals(changePropertyType(propertyType))) {
						createGetSetMethod(content, propertyName + "_ge", propertyType, propertyComment + "大于");
						createGetSetMethod(content, propertyName + "_le", propertyType, propertyComment + "小于");
					}
				}
			}
		}
		
		content.append("}").append("\n");
		
		// 写入文件
		writeFile(outFolder, packageName + ".model.condition", className + "SearchCondImpl.java", content); 
	}
	
	/**
	 * 生成DAO接口
	 * 
	 * @param outFolder
	 * @param packageName
	 * @param bean
	 */
	private void createDAO(String outFolder, String packageName, TableModelBean bean){
		StringBuffer content = new StringBuffer();
		String classComment = bean.getLogicalname();
		String className = toUpperCaseFirstOne(bean.getPhysicalname());
		content.append(getTitle());
		content.append("\n");
		content.append("package " + packageName + ".model.dao;");
		content.append("\n\r");
		content.append("import java.io.Serializable;");
		content.append("import java.util.List;");
		content.append("import  "  + BASEPACKAGE_FRAMEWORK + ".dao.hibernate.BaseHibernateDAO;");
		content.append("import " + packageName + ".model." + className + "Info;");
		content.append("import " + packageName + ".model.condition." + className + "SearchCond;");
		content.append("\n\r");
		content.append("/** \n");
		content.append(" * 概述: " + classComment + "DAO接口\n");
		content.append(" * 功能描述: " + classComment + "DAO接口\n");
		content.append(" * 创建记录: \n");
		content.append(" * 修改记录: \n");
		content.append(" */\n");
		content.append("public interface " + className + "DAO extends BaseHibernateDAO<" + className + "Info> {\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 全件检索").append("\n");
		content.append("     *").append("\n");
		content.append("     * @return 检索结果集").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> findAll();").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 主键检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * pk 主键").append("\n");
		content.append("     * @return 检索结果").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "Info findByPK(Serializable pk);").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索结果件数取得").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * @return 检索结果件数").append("\n");
		content.append("     */").append("\n");
		content.append("    public int getSearchCount(final " + className + "SearchCond cond);").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * start 检索记录起始行号").append("\n");
		content.append("     * count 检索记录件数").append("\n");
		content.append("     * @return 检索结果列表").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> findByCond(").append("\n");
		content.append("        final " + className + "SearchCond cond, final int start, final int count);").append("\n");
		
		content.append("}");
		
		writeFile(outFolder, packageName + ".model.dao", className + "DAO.java", content); 
	}
	
	/**
	 * 生成DAO实现
	 * 
	 * @param outFolder
	 * @param packageName
	 * @param bean
	 */
	private void createDAOImpl(String outFolder, String packageName, TableModelBean bean){
		StringBuffer content = new StringBuffer();
		String classComment = bean.getLogicalname();
		String className = toUpperCaseFirstOne(bean.getPhysicalname());
		content.append(getTitle());
		content.append("\n");
		content.append("package " + packageName + ".model.dao;");
		content.append("\n\r");
		content.append("import java.io.Serializable;").append("\n");
		content.append("import java.util.List;").append("\n");
		content.append("import org.hibernate.criterion.DetachedCriteria;").append("\n");
		content.append("import org.springframework.stereotype.Repository;").append("\n");
		content.append("import  "  + BASEPACKAGE_FRAMEWORK + ".dao.hibernate.BaseHibernateDAOImpl;").append("\n");
		content.append("import " + packageName + ".model." + className + "Info;").append("\n");
		content.append("import " + packageName + ".model.condition." + className + "SearchCond;").append("\n");
		content.append("import " + packageName + ".model.entity." + className + "TB;").append("\n");
		content.append("\n\r");
		content.append("/** \n");
		content.append(" * 概述: " + classComment + "DAO实现\n");
		content.append(" * 功能描述: " + classComment + "DAO实现\n");
		content.append(" * 创建记录: \n");
		content.append(" * 修改记录: \n");
		content.append(" */\n");
		if ("注解".equals(getAopType())) {
			content.append("@Repository(\"" + toLowerCaseFirstOne(className) + "DAO" + "\")").append("\n");
		}
		content.append("public class " + className + "DAOImpl extends").append("\n");
		content.append("        BaseHibernateDAOImpl<" + className + "Info> implements " + className + "DAO {").append("\n");

		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 构造函数").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "DAOImpl() {").append("\n");
		content.append("        super(" + className + "TB.class);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 全件检索").append("\n");
		content.append("     *").append("\n");
		content.append("     * @return 检索结果集").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> findAll() {").append("\n");
		content.append("        DetachedCriteria c = getDetachedCriteria();").append("\n");
		content.append("        return findByCriteria(c);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 主键检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * pk 主键").append("\n");
		content.append("     * @return 检索结果").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "Info findByPK(Serializable pk) {").append("\n");
		content.append("        " + className + "Info result = super.findByPK(pk);").append("\n");
		content.append("        if(result == null) return null;").append("\n");
		content.append("        return result;").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索结果件数取得").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * @return 检索结果件数").append("\n");
		content.append("     */").append("\n");
		content.append("    public int getSearchCount(final " + className + "SearchCond cond) {").append("\n");
		content.append("        DetachedCriteria c = getCriteria(cond);").append("\n");
		content.append("        return getCount(c);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * start 检索记录起始行号").append("\n");
		content.append("     * count 检索记录件数").append("\n");
		content.append("     * @return 检索结果列表").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> findByCond(").append("\n");
		content.append("        final " + className + "SearchCond cond, final int start, final int count) {").append("\n");
		content.append("        DetachedCriteria c = getCriteria(cond);").append("\n");
		content.append("        return findByCriteria(c, start, count);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 检索条件生成").append("\n");
		content.append("     * ").append("\n");
		content.append("     * @param cond 检索条件").append("\n");
		content.append("     * @return 检索用检索条件类").append("\n");
		content.append("     */").append("\n");
		content.append("    protected DetachedCriteria getCriteria(" + className + "SearchCond cond) {").append("\n");
		content.append("        DetachedCriteria c = getDetachedCriteria();").append("\n");
		content.append("        return c;").append("\n");
		content.append("    }").append("\n");
		
		content.append("}");
		
		writeFile(outFolder, packageName + ".model.dao", className + "DAOImpl.java", content); 
	}
	
	/**
	 * 生成taskService接口
	 * 
	 * @param outFolder
	 * @param packageName
	 * @param bean
	 */
	private void createTaskService(String outFolder, String packageName, TableModelBean bean, Integer complexPK){
		StringBuffer content = new StringBuffer();
		String classComment = bean.getLogicalname();
		String className = toUpperCaseFirstOne(bean.getPhysicalname());
		content.append(getTitle());
		content.append("\n");
		content.append("package " + packageName + "." + BASEPACKAGE_SERVICE + ".task;");
		content.append("\n\r");
//		content.append("import java.io.Serializable;").append("\n");
		content.append("import java.util.List;").append("\n");
		content.append("import "  + BASEPACKAGE_FRAMEWORK + ".base.BLException;").append("\n");
		content.append("import "  + BASEPACKAGE_FRAMEWORK + ".base.Base" + BASENAME_SERVICE + ";").append("\n");
		content.append("import " + packageName + ".model." + className + "Info;").append("\n");
		content.append("import " + packageName + ".model.condition." + className + "SearchCond;").append("\n");
		if (complexPK > 1) {
			content.append("import " + packageName + ".model.entity." + className + "PK;").append("\n");
		}
		content.append("import " + packageName + ".model.entity." + className + "TB;").append("\n");
		content.append("\n\r");
		content.append("/** \n");
		content.append(" * 概述: " + classComment + "业务逻辑接口\n");
		content.append(" * 功能描述: " + classComment + "业务逻辑接口\n");
		content.append(" * 创建记录: \n");
		content.append(" * 修改记录: \n");
		content.append(" */\n");
		content.append("public interface " + className + BASENAME_SERVICE + " extends BaseService {\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * " + classComment + "实例取得").append("\n");
		content.append("     *").append("\n");
		content.append("     * @return " + classComment + "实例").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "TB " + "get" + className + "TBInstance();").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 全件检索").append("\n");
		content.append("     *").append("\n");
		content.append("     * @return 检索结果集").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> search" + className + "();").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 主键检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * pk 主键").append("\n");
		content.append("     * @return 检索结果").append("\n");
		content.append("     */").append("\n");
		if( complexPK <= 1) {
			Map<String, String> pkMap = bean.getPkMap();
			String pkType = "";
			for (Map.Entry<String, String> entry : pkMap.entrySet()) {
				pkType = changePropertyType(entry.getValue());
			}
			content.append("    public " + className + "Info search" + className + "ByPK(" + pkType + " pk)").append("\n");
		} else {
			content.append("    public " + className + "Info search" + className + "ByPK(" + className + "PK pk)").append("\n");
		}
		content.append("        throws BLException;").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索结果件数取得").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * @return 检索结果件数").append("\n");
		content.append("     */").append("\n");
		content.append("    public int getSearchCount(" + className + "SearchCond cond);").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * start 检索记录起始行号").append("\n");
		content.append("     * count 检索记录件数").append("\n");
		content.append("     * @return 检索结果列表").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> search" + className + "(").append("\n");
		content.append("        " + className + "SearchCond cond);").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * start 检索记录起始行号").append("\n");
		content.append("     * count 检索记录件数").append("\n");
		content.append("     * @return 检索结果列表").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> search" + className + "(").append("\n");
		content.append("        " + className + "SearchCond cond, int start, int count);").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 新增处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param instance 新增实例").append("\n");
		content.append("     * @return 新增实例").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "Info regist" + className + "(" + className + "Info instance)").append("\n");
		content.append("        throws BLException;").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 修改处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param instance 修改实例").append("\n");
		content.append("     * @return 修改实例").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "Info update" + className + "(" + className + "Info instance)").append("\n");
		content.append("        throws BLException;").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 删除处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param pk 主键").append("\n");
		content.append("     */").append("\n");
		if( complexPK <= 1) {
			Map<String, String> pkMap = bean.getPkMap();
			String pkType = "";
			for (Map.Entry<String, String> entry : pkMap.entrySet()) {
				pkType = changePropertyType(entry.getValue());
			}
			content.append("    public void delete" + className + "ByPK(" + pkType + " pk)").append("\n");
		} else {
			content.append("    public void delete" + className + "ByPK(" + className + "PK pk)").append("\n");
		}
		content.append("        throws BLException;").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 删除处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param instance 删除实例").append("\n");
		content.append("     */").append("\n");
		content.append("    public void delete" + className + "(" + className + "Info instance)").append("\n");
		content.append("        throws BLException;").append("\n");
		
		content.append("}");
		
		writeFile(outFolder, packageName + ".service.task", className + "Service.java", content); 
	}
	
	/**
	 * 生成taskService接口
	 * 
	 * @param outFolder
	 * @param packageName
	 * @param bean
	 */
	private void createTaskServiceImpl(String outFolder, String packageName, TableModelBean bean, Integer complexPK){
		StringBuffer content = new StringBuffer();
		String classComment = bean.getLogicalname();
		String className = toUpperCaseFirstOne(bean.getPhysicalname());
		String daoName = toLowerCaseFirstOne(className) + "DAO";

		content.append(getTitle());
		content.append("\n");
		content.append("package " + packageName + "." + BASEPACKAGE_SERVICE + ".task;");
		content.append("\n\r");
		content.append("import java.util.List;").append("\n");
		content.append("import javax.annotation.Resource;").append("\n");
		content.append("import org.springframework.stereotype.Service;").append("\n");
		content.append("import "  + BASEPACKAGE_FRAMEWORK + ".base.BLException;").append("\n");
		content.append("import "  + BASEPACKAGE_FRAMEWORK + ".base.Base" + BASENAME_SERVICE + "Impl;").append("\n");
		content.append("import " + packageName + ".model." + className + "Info;").append("\n");
		content.append("import " + packageName + ".model.condition." + className + "SearchCond;").append("\n");
		content.append("import " + packageName + ".model.dao." + className + "DAO;").append("\n");
		if (complexPK > 1) {
			content.append("import " + packageName + ".model.entity." + className + "PK;").append("\n");
		}
		content.append("import " + packageName + ".model.entity." + className + "TB;").append("\n");
		content.append("\n\r");
		content.append("/** \n");
		content.append(" * 概述: " + classComment + "业务逻辑实现\n");
		content.append(" * 功能描述: " + classComment + "业务逻辑实现\n");
		content.append(" * 创建记录: \n");
		content.append(" * 修改记录: \n");
		content.append(" */\n");
		if ("注解".equals(getAopType())) {
			content.append("@Service(\"" + toLowerCaseFirstOne(className) + BASENAME_SERVICE + "\")").append("\n");
		}
		content.append("public class " + className + BASENAME_SERVICE + "Impl extends Base" + BASENAME_SERVICE + "Impl implements\n");
		content.append("        " + className + BASENAME_SERVICE +" {\n");
		
		content.append("\r");
		content.append("    /** " + classComment + "DAO接口 */").append("\n");
		if ("注解".equals(getAopType())) {
			content.append("    @Resource").append("\n");
		}
		content.append("    protected " + className + "DAO " + daoName + ";").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * " + classComment + "DAO接口设定").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param " + daoName + " " + classComment + "DAO接口").append("\n");
		content.append("     */").append("\n");
		content.append("    public void set" + className + "DAO(" + className + "DAO " + daoName + ") {").append("\n");
		content.append("        this." + daoName + " = " + daoName + ";").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * " + classComment + "实例取得").append("\n");
		content.append("     *").append("\n");
		content.append("     * @return " + classComment + "实例").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "TB " + "get" + className + "TBInstance() {").append("\n");
		content.append("        return new " + className + "TB();").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 全件检索").append("\n");
		content.append("     *").append("\n");
		content.append("     * @return 检索结果集").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> search" + className + "() {").append("\n");
		content.append("        return " + daoName + ".findAll();").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 主键检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * pk 主键").append("\n");
		content.append("     * @return 检索结果").append("\n");
		content.append("     */").append("\n");
		if( complexPK <= 1) {
			Map<String, String> pkMap = bean.getPkMap();
			String pkType = "";
			for (Map.Entry<String, String> entry : pkMap.entrySet()) {
				pkType = changePropertyType(entry.getValue());
			}
			content.append("    public " + className + "Info search" + className + "ByPK(" + pkType + " pk)").append("\n");
		} else {
			content.append("    public " + className + "Info search" + className + "ByPK(" + className + "PK pk)").append("\n");
		}
		content.append("        throws BLException {").append("\n");
		content.append("        return " + "checkExistInstance(pk);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索结果件数取得").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * @return 检索结果件数").append("\n");
		content.append("     */").append("\n");
		content.append("    public int getSearchCount(" + className + "SearchCond cond) { ").append("\n");
		content.append("        return " + daoName + ".getSearchCount(cond);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * start 检索记录起始行号").append("\n");
		content.append("     * count 检索记录件数").append("\n");
		content.append("     * @return 检索结果列表").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> search" + className + "(").append("\n");
		content.append("        " + className + "SearchCond cond) {").append("\n");
		content.append("        return " + daoName + ".findByCond(cond, 0, 0);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 条件检索处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * cond 检索条件").append("\n");
		content.append("     * start 检索记录起始行号").append("\n");
		content.append("     * count 检索记录件数").append("\n");
		content.append("     * @return 检索结果列表").append("\n");
		content.append("     */").append("\n");
		content.append("    public List<" + className + "Info> search" + className + "(").append("\n");
		content.append("        " + className + "SearchCond cond, int start, int count) {").append("\n");
		content.append("        return " + daoName + ".findByCond(cond, start, count);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 新增处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param instance 新增实例").append("\n");
		content.append("     * @return 新增实例").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "Info regist" + className + "(" + className + "Info instance)").append("\n");
		content.append("        throws BLException {").append("\n");
		content.append("        return " + daoName + ".save(instance);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 修改处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param instance 修改实例").append("\n");
		content.append("     * @return 修改实例").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "Info update" + className + "(" + className + "Info instance)").append("\n");
		content.append("        throws BLException {").append("\n");
		
		StringBuffer pkcontent = new StringBuffer();
		Map<String, String> pkMap = bean.getPkMap();
		if( complexPK <= 1) {
			String pkPro = "";
			for (Map.Entry<String, String> entry : pkMap.entrySet()) {
				pkPro = toUpperCaseFirstOne(entry.getKey());
			}
			pkcontent.append("instance.get" + pkPro + "()");
			content.append("        checkExistInstance(" + pkcontent + ");").append("\n");
		} else if( complexPK > 1) {
			for (Map.Entry<String, String> entry : pkMap.entrySet()) {
				String pkPro = toUpperCaseFirstOne(entry.getKey());
				pkcontent.append("instance.get" + pkPro + "(), ");
			}
			content.append("        checkExistInstance(new " + className + "PK(").append(pkcontent.substring(0, pkcontent.lastIndexOf(","))).append(");").append("\n");
		}
		content.append("        return " + daoName + ".save(instance);").append("\n");
		content.append("    }");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 删除处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param pk 主键").append("\n");
		content.append("     */").append("\n");
		if( complexPK <= 1) {
			String pkType = "";
			for (Map.Entry<String, String> entry : pkMap.entrySet()) {
				pkType = changePropertyType(entry.getValue());
			}
			content.append("    public void delete" + className + "ByPK(" + pkType + " pk)").append("\n");
		} else {
			content.append("    public void delete" + className + "ByPK(" + className + "PK pk)").append("\n");
		}
		content.append("        throws BLException {").append("\n");
		content.append("        " + daoName + ".delete(checkExistInstance(pk));").append("\n");
		content.append("    }").append("\n");

		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 删除处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param instance 删除实例").append("\n");
		content.append("     */").append("\n");
		content.append("    public void delete" + className + "(" + className + "Info instance)").append("\n");
		content.append("        throws BLException {").append("\n");
		content.append("        " + daoName + ".delete(instance);").append("\n");
		content.append("    }").append("\n");
		
		content.append("\r");
		content.append("    /**").append("\n");
		content.append("     * 实例存在检查处理").append("\n");
		content.append("     *").append("\n");
		content.append("     * @param pk 主键").append("\n");
		content.append("     * @return 实例").append("\n");
		content.append("     */").append("\n");
		if( complexPK <= 1) {
			String pkType = "";
			for (Map.Entry<String, String> entry : pkMap.entrySet()) {
				pkType = changePropertyType(entry.getValue());
			}
			content.append("    public " + className + "Info checkExistInstance(" + pkType + " pk)").append("\n");
		} else if( complexPK > 1){
			content.append("    public " + className + "Info checkExistInstance(" + className + "PK pk)").append("\n");
		}
		content.append("        throws BLException {").append("\n");
		content.append("        " + className + "Info instance = " + daoName + ".findByPK(pk);").append("\n");
		content.append("        if (instance == null) {").append("\n");
		content.append("            throw new BLException(\"IGCO10000.E004\", \" 实例基本\");").append("\n");
		content.append("        }").append("\n");
		content.append("        return instance;").append("\n");
		content.append("    }").append("\n");
		
		content.append("}");
		
		writeFile(outFolder, packageName + ".service.task", className + "ServiceImpl.java", content); 
	}
	
	/**
	 * 生产复合主键
	 * 
	 * @param outFolder
	 * @param packageName
	 * @param bean
	 * @param complexPK
	 */
	private void createPK(String outFolder, String packageName, TableModelBean bean){
		Vector<Vector<Object>> data = bean.getData();
		
		StringBuffer content = new StringBuffer();
		String classComment = bean.getLogicalname();
		String className = toUpperCaseFirstOne(bean.getPhysicalname());
		content.append(getTitle());
		content.append("\n");
		content.append("package " + packageName + ".model.entity;");
		content.append("\n\r");
		content.append("import java.io.Serializable;\n");
		content.append("import  "  + BASEPACKAGE_FRAMEWORK + ".base.BasePK;").append("\n");
		content.append("\r");
		content.append("/**");
		content.append(" * 概述: " + classComment + "实体主键");
		content.append(" * 创建记录: ");
		content.append(" */");
		content.append("@SuppressWarnings(\"serial \")").append("\n");
		content.append("public class " + className + "PK extends BasePK implements Serializable {");
		content.append("\r");
		
		StringBuffer argsb = new StringBuffer();		// 定义参数列表
		StringBuffer setValuesb = new StringBuffer();	// 定义赋值列表
		StringBuffer methodsb = new StringBuffer();		// 定义方法
		// 生产属性
		for (Vector<Object> vector : data) {
			String propertyName = vector.get(1).toString();//定义属性名
			String propertyType = vector.get(2).toString(); //定义属性类型
			String propertyComment = vector.get(0).toString();  //定义属性注释
			String propertyPK = vector.get(4).toString().trim();  //定义主键
			if (StringUtils.isNotEmpty(propertyName) && StringUtils.isNotEmpty(propertyType) && StringUtils.isNotEmpty(propertyPK)
					&& StringUtils.isNotEmpty(propertyComment) && !"CREATETIME".equals(propertyName.toUpperCase()) 
					&& !"UPDATETIME".equals(propertyName.toUpperCase())) {
				content.append("\r");
				content.append("    /** " + propertyComment + " */").append("\n");
				content.append("    protected " + changePropertyType(propertyType) + " " + propertyName + ";").append("\n");
				
				argsb.append(changePropertyType(propertyType) + " " + propertyName + ", ");
				setValuesb.append("        this." + propertyName + " = " + propertyName + ";").append("\n");
				
				createGetSetMethod(methodsb, propertyName, propertyType, propertyComment);
			}
		}
		content.append("\r");
		
		// 无参构造方法
		content.append("    /**").append("\n");
		content.append("     * 构造函数").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "PK() {").append("\n");
		content.append("\r");
		content.append("    }").append("\n");
		content.append("\r");
		
		// 有参构造参数
		content.append("    /**").append("\n");
		content.append("     * 构造函数").append("\n");
		content.append("     */").append("\n");
		content.append("    public " + className + "PK(" + argsb.substring(0, argsb.lastIndexOf(",")) + ") {").append("\n");
		content.append(setValuesb);
		content.append("    }").append("\n");
		
		// 添加get set 方法
		content.append(methodsb);
		content.append("}");
		
		// 写入文件
		writeFile(outFolder, packageName + ".model.entity", className + "PK.java", content); 
	}

	/**
	 * 生成GetSet方法
	 * 
	 * @param content
	 * @param propertyName
	 * @param propertyType
	 * @param propertyComment
	 */
	private void createGetSetMethod(StringBuffer content, String propertyName,
			String propertyType, String propertyComment) {
		content.append("\r");
		//生成get方法
		content.append("    /**").append("\n");
		content.append("     *" + propertyComment + "取得").append("\n");
		content.append("     *").append("\n");
		content.append("     *@return " + propertyComment).append("\n");
		content.append("     */").append("\n");
		content.append("    public " + changePropertyType(propertyType) + " get" + toUpperCaseFirstOne(propertyName) + "() {").append("\n");
		content.append("        return " + propertyName + ";").append("\n");
		content.append("    }").append("\n");
		content.append("\r");
		
		//生成set方法
		content.append("    /**").append("\n");
		content.append("     * " + propertyComment + "设定").append("\n");
		content.append("     * ").append("\n");
		content.append("     * @param " + propertyName + " " + propertyComment).append("\n");
		content.append("     */").append("\n");
		content.append("    public void set" + toUpperCaseFirstOne(propertyName) + "(" + changePropertyType(propertyType) + " " + propertyName + ") {").append("\n");
		content.append("        this." + propertyName + " = " + propertyName + ";").append("\n");
		content.append("    }").append("\n");
	}
	
	private void createApplicationXML(String outFolder, String packageName, List<TableModelBean> lst){
		Document document = DocumentHelper.createDocument();  
        Element root = document.addElement("beans");//添加文档根 
        // 生成dao
        for (TableModelBean bean : lst) {
        	String classComment = bean.getLogicalname();
        	String className = toUpperCaseFirstOne(bean.getPhysicalname());
        	root.addComment(classComment + "DAO");//加入一行注释  
        	Element xmlbean = root.addElement("bean");
        	xmlbean.addAttribute("id", toLowerCaseFirstOne(className) + "DAO");  
        	xmlbean.addAttribute("parent", "baseHibernateDAO");  
        	xmlbean.addAttribute("class", packageName + ".model.dao." + className + "DAOImpl");  
		}
        
        // 生成TaskBL
        for (TableModelBean bean : lst) {
        	String classComment = bean.getLogicalname();
        	String className = toUpperCaseFirstOne(bean.getPhysicalname());
        	root.addComment(classComment + "TaskBL");//加入一行注释  
        	Element xmlbean = root.addElement("bean");
        	xmlbean.addAttribute("id", toLowerCaseFirstOne(className) + "BL");  
        	xmlbean.addAttribute("class", packageName + ".bl.task." + className + "BLImpl"); 
        	Element property = xmlbean.addElement("property");
        	property.addAttribute("name", toLowerCaseFirstOne(className) + "DAO");
        	Element ref = property.addElement("ref");
        	ref.addAttribute("bean", toLowerCaseFirstOne(className) + "DAO");
        }
        
        String folderStr = outFolder + File.separator + packageName.replace(".", File.separator) + File.separator ;
		File folder = new File(folderStr);
		if (!folder.exists()) {
			folder.mkdirs();
		}
		folder = null;
		// 格式化XML,  true代表是否换行  
        OutputFormat format = new OutputFormat("    ",true);  
        format.setEncoding(getCharacterSet());//设置编码格式
        XMLWriter xmlWriter = null;
        OutputStreamWriter out = null;
		try {
			out = new OutputStreamWriter(
					new FileOutputStream(folderStr + "applicationContext.xml")  , getCharacterSet());
			out.write("<!DOCTYPE beans PUBLIC \"-//SPRING//DTD BEAN//EN\"");
			out.write(" http://www.springframework.org/dtd/spring-beans.dtd\"> \n");
			xmlWriter = new XMLWriter(out, format);
			xmlWriter.write(document);  
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				xmlWriter.close();
				out.close();
			} catch (IOException e) {
			}  
		}
      
	}
	
	/**
	 * 写入文件
	 * 
	 * @param outFolder
	 * @param packageName
	 * @param className
	 * @param content
	 */
	private void writeFile(String outFolder, String packageName,
			String className, StringBuffer content) {
		String folderStr = outFolder + File.separator + packageName.replace(".", File.separator) + File.separator ;
		File folder = new File(folderStr);
		if (!folder.exists()) {
			folder.mkdirs();
		}
		folder = null;
		
		try {
			OutputStreamWriter out = new OutputStreamWriter(
					new FileOutputStream(folderStr + className)  , getCharacterSet());
			out.write(content.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取文件标题
	 * 
	 * @return
	 */
	private String getTitle(){
		StringBuffer title = new StringBuffer();
		title.append("/* \n");
		title.append(" * " + BASEPACKAGE + "版权所有，保留所有权利。\n");
		title.append(" */ \n\t");
		return title.toString();
	}
	
	/**
	 * 将数据库类型转化为java类型
	 */
	private String changePropertyType(String propertyType){
		if ("CHAR".equals(propertyType.toUpperCase()) 
				|| propertyType.toUpperCase().startsWith("VARCHAR")) {
			propertyType = "String";
		} else if ("INTEGER".equals(propertyType.toUpperCase()) 
				|| "NUMBER".equals(propertyType.toUpperCase())) {
			propertyType = "Integer";
		} else if ("DECIMAL".equals(propertyType.toUpperCase())) {
			propertyType = "Decimal";
		} else if ("DATE".equals(propertyType.toUpperCase())) {
			propertyType = "Date";
		} else if ("TIMESTAMP".equals(propertyType.toUpperCase())) {
			propertyType = "Date";
		}
		return propertyType;
	}
	
	/**
	 * 首字母转大写
	 * 
	 * @param name
	 * @return
	 */
	public String toUpperCaseFirstOne(String name) {
		char[] ch = name.toCharArray();
		ch[0] = Character.toUpperCase(ch[0]);
		StringBuffer a = new StringBuffer();
		a.append(ch);
		return a.toString();
	}
	
	/**
	 * 首字母转小写
	 * 
	 * @param name
	 * @return
	 */
	public String toLowerCaseFirstOne(String name) {
		char[] ch = name.toCharArray();
		ch[0] = Character.toLowerCase(ch[0]);
		StringBuffer a = new StringBuffer();
		a.append(ch);
		return a.toString();
	}
	
	
	public static void main(String[] args) throws BiffException, IOException {
		MainUIService rts = new MainUIService();
		rts.setCharacterSet("UTF-8");
		List<TableModelBean> datalst = rts.readTemplate("D:\\文件\\工作文件\\简洛\\test.xls");
		rts.createCode("D:\\a", "com.deliverik.infogovernor.sym", datalst);
		System.out.println(datalst);
				
	}
}
