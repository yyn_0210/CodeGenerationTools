package net.janeluo.tools.bean;

import java.util.Map;
import java.util.Vector;

public class TableModelBean {

	protected String logicalname;
	
	protected String physicalname;
	
	protected Vector<Vector<Object>> data;
	
	protected Vector<Object> columnNames;

	protected Map<String, String> pkMap;
	
	public String getLogicalname() {
		return logicalname;
	}

	public void setLogicalname(String logicalname) {
		this.logicalname = logicalname;
	}

	public String getPhysicalname() {
		return physicalname;
	}

	public void setPhysicalname(String physicalname) {
		this.physicalname = physicalname;
	}

	public Vector<Vector<Object>> getData() {
		return data;
	}

	public void setData(Vector<Vector<Object>> data) {
		this.data = data;
	}

	public Vector<Object> getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(Vector<Object> columnNames) {
		this.columnNames = columnNames;
	}

	public Map<String, String> getPkMap() {
		return pkMap;
	}

	public void setPkMap(Map<String, String> pkMap) {
		this.pkMap = pkMap;
	}
	
}
